import Vue from 'vue';
import App from '@/App';
import vuetify from './plugins/vuetify'


Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.config.debug = true;

new Vue({
  vuetify,
  render: (h) => h(App)
}).$mount("#vue-wc");