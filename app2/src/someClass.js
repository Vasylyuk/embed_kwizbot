import Vue from 'vue';
import App from '@/App';
import vuetify from './plugins/vuetify'
import VueRouter from 'vue-router';
import axios from "axios";
import Page1 from "@/views/Page1";
import Page2 from "@/views/Page2";


Vue.use(VueRouter)
const router = new VueRouter({
  base: '/kw',
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/page1',
    },
    {
      path: '/page1',
      component: Page1,
    },
    {
      path: '/page2',
      component: Page2,
    },
  ],
});


axios.interceptors.response.use(
  function (response) {
    console.log('App2 axios response =>', response);
    return response
  }
);

Vue.config.productionTip = false;

const Instance = new Vue({
  vuetify,
  components: { App },
  data: () => ({ config: {} }),
  render: function(createElement) {
    return createElement('app', {
      props: { config: this.config }
    });
  }
});

export default class SomeClass {
  constructor(selector, config) {
    Vue.set(Instance, 'config', {
      ...config,
      // id is optional. but allows setting the app's `id` dynamically to same `id`
      // as the context page placeholder (which app replaces), for consistency
      // for this, you have to assign `config.id` to App's `$el` in its `mounted()`
      id: selector.replace('#', '')
      // or document.querySelector(selector).id || 'someClassApp'
    });
    Instance.$mount(selector);
    return Instance;
  }
}

// optional exports, used for type inheritance throughout the app
export const { $http, $t } = Instance;