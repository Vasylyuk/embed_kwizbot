import logo from './logo.svg';
import './App.css';
import { useRoutes, Outlet } from 'react-router';
import { BrowserRouter } from "react-router-dom";
import Layout from "./pages/Layout";
import Home from './pages/Home';
import Page1 from './pages/Page1';
import Page2 from './pages/Page2';

const DashboardApp = () => {
  return useRoutes(routes);
};
const Yuu = () => {
  return <Outlet />
}

const routes = [
  {
    // element: <App />,
    path: '/',
    element: <Yuu />,
    children: [
      // {
      //   path: '/',
      //   element: <Home />,
      // },
      {
        path: 'page1',
        element: <Page1 />,
      },
      {
        path: `kw/page2`,
        element: <Page2 />,
      },
    ],
  },
];

//useRoutes HOOK

function App() {

  return (
    <div className="App">
      <menu>
        <BrowserRouter>
          <DashboardApp />
          {/*<Routes>*/}
          {/*  <Route path="/" element={<Layout />}>*/}
          {/*    <Route path="page1" element={<Page1 />} />*/}
          {/*    <Route path="page2" element={<Page2 />} />*/}
          {/*  </Route>*/}
          {/*</Routes>*/}
        </BrowserRouter>
      </menu>


      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>

      <br/>
      <br/>
      <br/>
      <br/>
      <div>
        <span>1</span>
        <span>2</span>
        <span>4</span>
        <div id="vue-wc"></div>
      </div>
    </div>
  );
}

export default App;


