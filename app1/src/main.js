import Vue from 'vue';
import VueRouter from 'vue-router';
import App from '@/App';
import Page1 from '@/views/Page1';
import Page2 from '@/views/Page2';
import axios from "axios";

// import '../../app2/dist/operator-panel.min';
// import '../../app2/dist/js/app.3e9da9a2';
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false;
Vue.config.devtools = true;
Vue.config.debug = true;

Vue.use(VueRouter);

axios.interceptors.response.use(
    function (response) {
        console.log('App1 axios response =>', response);
        return response
    }
);

export const router = new VueRouter({
    base: '',
    mode: 'history',
    routes: [
        // {
        //     path: '/page2',
        //     // component: () => import('@/views/Page1'),
        //     component: Page2,
        // },
        {
            path: '/page1',
            name: 'Page1',
            // component: () => import('@/views/Page2'),
            component: Page1,

            children: [
                {
                    path: 'kw',
                    name: 'KW',
                    // component: () => import('@/views/Page2'),
                    component: Page2,
                },
            ]

        },


    ],
});

new Vue({
    router,
    vuetify,
    render: (h) => h(App)
}).$mount("#app");
