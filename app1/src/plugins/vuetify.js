import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    options: { customProperties: true },
    themes: {
      light: {
        primary: '#5567FF',
        primaryLighten: '#5784eb',
        primaryLight: '#D6DCFA',
        primaryLighten1: '#8794FF',
        primaryBackground: '#F3F6FB',
        primaryButtonBg: '#C2D2F8',
        primaryBlue: '#5567FF',
        primaryBlueDark: '#537cdb',
        secondary: '#050038', // #FFCDD2
        gray: '#B0BEC8', // #FFCDD2
        gray1: '#AFAFAF',
        gray2: "#f8f8f8",
        gray4: '#F8F8F8',
        accent: '#767396', // #3F51B5
        greyBlueLight: '#AEAEC5',
        greyPurple: '#6C6C8B',
        red: '#F51C5D',
        black2: '#292931',
        orangeLight1: "#fff0de",

      },
    },
  },
});
